<?php

return [
    'enabled' => env('GOOGLE_FIT_ENABLED', false),
    'oauth_client_id' => env('GOOGLE_FIT_OAUTH_CLIENT_ID'),
    'oauth_secret' => env('GOOGLE_FIT_OAUTH_SECRET')
];
