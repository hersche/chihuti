<?php

namespace App\Http\Controllers;

use App\Device;
use App\DeviceInstance;
use App\Sensor;
use App\SensorInstance;
use App\SensorValue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Fitness;
use Illuminate\Support\Facades\Auth;

class GoogleFit extends Controller
{
    const ACCESS_TOKEN_SESSION_KEY = 'fit_access_token';
    private $client;
    private $authorised = false;
    public function __construct()
    {
        $this->client = new Google_Client();
        $oauth_client = config('fit.oauth_client_id'); // "420381869185-giqdo4h1c8fg4jajjjcp5uqm39cevnf7.apps.googleusercontent.com";
        $oauth_secret = config('fit.oauth_secret'); //"rwnNexAUTr35VVQQaCucD80G";
        $this->client = new Google_Client();
        $this->client->setApplicationName('voicy');
        $this->client->setApprovalPrompt("auto");
        $this->client->setAccessType('online');
//        $client->setAccessToken('AIzaSyCa1HWFOq8G40MG44szunA_BBQyCuhlTac');
        $this->client->setClientId($oauth_client);
        $this->client->setClientSecret($oauth_secret);

        $redirect_uri = config('app.url').'/fit';
//echo $redirect_uri; die();
        $this->client->setRedirectUri($redirect_uri);
        $this->client->addScope(Google_Service_Fitness::FITNESS_ACTIVITY_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_BLOOD_PRESSURE_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_LOCATION_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_BODY_TEMPERATURE_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_OXYGEN_SATURATION_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_REPRODUCTIVE_HEALTH_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_BLOOD_GLUCOSE_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_BODY_READ);
        $this->client->addScope(Google_Service_Fitness::FITNESS_NUTRITION_READ);
        if(!empty(session()->get(self::ACCESS_TOKEN_SESSION_KEY))){
            $this->client->setAccessToken($this->client->fetchAccessTokenWithAuthCode(session()->get(self::ACCESS_TOKEN_SESSION_KEY)));
            $this->authorised = true;
        }
    }

    public function getCSV(Request $request){

        $timezone = "GMT+0100";
        $today = date("Y-m-d");
        $endTime = strtotime($today.' 00:00:00 '.$timezone);
        $startTime = strtotime('-2 day', $endTime);

        if (isset($_GET['code']) && !$this->authorised) {
            session()->put(self::ACCESS_TOKEN_SESSION_KEY, $this->client->fetchAccessTokenWithAuthCode($_GET['code']));
            $this->client->setAccessToken(session()->get(self::ACCESS_TOKEN_SESSION_KEY));
            $this->authorised = true;
        }

        if(!$this->authorised || !Auth::check()){
            return response()->json([],401);
        }

        $fit = new \Google_Service_Fitness($this->client);
        if (isset($_GET['code'])) {
            $dataSources = $fit->users_dataSources->listUsersDataSources('me');
            $dataSets = $fit->users_dataSources_datasets;
            $data = [];
            while ($dataSources->valid()){
                $item = $dataSources->next();
                if($item != false) {
                    echo ''.$item['dataType']['name'].'('.$item['dataStreamName'].')<br />';
                    var_dump($item['device']);
                    echo "<br />";
                    $device = Device::firstOrCreate(['model' => $item['device']['model'], 'name' => $item['application']['name']]);
                    $deviceInstance = DeviceInstance::firstOrCreate(['alias' => $item['device']['model'], 'device_id' => $device->id]);

                    $typeName = $item['dataType']['field'][0]['name'];
                    $dataType = $item['dataType']['field'][0]['format'];
                    $sensor = Sensor::firstOrCreate(['appendix' => $typeName, 'name' => $item['dataType']['name'], 'body_related' => true, 'device_id' => $device->id]);
                    $sensorInstance = SensorInstance::firstOrCreate(['user_id' => Auth::id(), 'sensor_id' => $sensor->id, 'device_instance_id' => $deviceInstance->id]);
                    echo "<br />";
                    echo $dataType;
                    if($dataType=='integer'){
                        $dataType='intVal';
                    }
                    if($dataType=='floatPoint'){
                        $dataType='fpVal';
                    }

                    echo "<br />";
                    $dataStreamId = $item['dataStreamId'];
                    $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$endTime.'000000000');
                    while($listDatasets->valid()) {
                        $dataSet = $listDatasets->next();
                        if($dataSet!= false){
                            $dataSetValues = $dataSet['value'];
                            echo date( 'r', substr($dataSet['startTimeNanos'],0,10)).'-';
                            echo date( 'r', substr($dataSet['endTimeNanos'],0,10)).': ';
                            if ($dataSetValues && is_array($dataSetValues)) {
                                foreach($dataSetValues as $dataSetValue) {
                                    if($dataSetValue != false){
                                        //if($dataType=='intVal'){
                                        SensorValue::firstOrCreate([
                                                                       'started_at' => date( 'r', substr($dataSet['startTimeNanos'],0,10)),
                                                                       'ended_at' => date( 'r', substr($dataSet['endTimeNanos'],0,10)),
                                                                       'sensor_instance_id' => $sensorInstance->id,
                                                                       'value' => $dataSetValue[$dataType]
                                                                   ]);
                                        echo($dataSetValue[$dataType].' '.$typeName);
                                        //} else {
                                        //var_dump($dataSetValue);
                                        //}
                                    }
                                }
                            }
                        }
                        echo '<br />';
                    }
                    echo "<br />";
                }
            }

        }
        return redirect($this->client->createAuthUrl());
        //      var_dump($fit->users_dataSources->listUsersDataSources('me'));
    }

    //
    public function get(Request $request){

        $timezone = "GMT+0100";
        $today = date("Y-m-d");
        $endTime = strtotime($today.' 00:00:00 '.$timezone);
        $startTime = strtotime('-2 day', $endTime);

        if (isset($_GET['code']) && !$this->authorised) {
            session()->put(self::ACCESS_TOKEN_SESSION_KEY, $_GET['code']);
            $this->client->setAccessToken($this->client->fetchAccessTokenWithAuthCode(session()->get(self::ACCESS_TOKEN_SESSION_KEY)));
            $this->authorised = true;
        }


        $fit = new \Google_Service_Fitness($this->client);
        if ($this->authorised) {
            $dataSources = $fit->users_dataSources->listUsersDataSources('me');
            $dataSets = $fit->users_dataSources_datasets;
            $data = [];
            $theData = ['google_fit_auth_url' => ''];
            while ($dataSources->valid()){
                $item = $dataSources->next();
                if($item != false) {

                    $model = 'none';
                    if(!empty($item['device']) && !empty($item['device']['model'])){
                        $model = $item['device']['model'];
                    }
                    $app = 'none';
                    if(!empty($item['application']) && !empty($item['application']['name'])){
                        $app = $item['application']['name'];
                    }
                    $device = Device::firstOrCreate(['model' => $model, 'name' => $app]);
                    $deviceInstance = DeviceInstance::firstOrCreate(['alias' => $model, 'device_id' => $device->id]);

                    $typeName = $item['dataType']['field'][0]['name'];
                    $dataType = $item['dataType']['field'][0]['format'];
                    $sensor = Sensor::firstOrCreate(['appendix' => $typeName, 'name' => $item['dataType']['name'], 'body_related' => true, 'device_id' => $device->id]);

                    $sensorInstance = SensorInstance::firstOrCreate(['user_id' => Auth::id(), 'sensor_id' => $sensor->id, 'device_instance_id' => $deviceInstance->id]);
                    if(!isset($theData[$item['dataStreamName']])){
                        $theData[$item['dataStreamName']] = [
                            'device' => $device,
                            'deviceInstance' => $deviceInstance,
                            'sensor' => $sensor,
                            'sensorInstance' => $sensorInstance,
                            'values' => []
                        ];
                    }
                    if($dataType=='integer'){
                        $dataType='intVal';
                    }
                    if($dataType=='floatPoint'){
                        $dataType='fpVal';
                    }

                    $dataStreamId = $item['dataStreamId'];
                    $listDatasets = $dataSets->get("me", $dataStreamId, $startTime.'000000000'.'-'.$endTime.'000000000');
                    while($listDatasets->valid()) {
                        $dataSet = $listDatasets->next();
                        if($dataSet!= false){
                            $dataSetValues = $dataSet['value'];
                            if ($dataSetValues && is_array($dataSetValues)) {
                                foreach($dataSetValues as $dataSetValue) {
                                    if($dataSetValue != false){
                                        //if($dataType=='intVal'){
                                        $sensorValue = SensorValue::firstOrCreate([
                                            'started_at' => date( 'Y-m-d H:i:s', substr($dataSet['startTimeNanos'],0,10)),
                                            'ended_at' => date( 'Y-m-d H:i:s', substr($dataSet['endTimeNanos'],0,10)),
                                            'sensor_instance_id' => $sensorInstance->id,
                                            'value' => $dataSetValue[$dataType]
                                                                   ]);
                                        $theData[$item['dataStreamName']]['values'][] = $sensorValue;
                                        //} else {
                                        //var_dump($dataSetValue);
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response()->json($theData);
        }
        return response()->json(['google_fit_auth_url' => $this->client->createAuthUrl()]);
    }

    public function getAuthLink(){
        return response()->json(['google_fit_auth_url' => $this->client->createAuthUrl()]);
    }
}
