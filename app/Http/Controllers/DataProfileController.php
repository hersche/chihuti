<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\DataProfile;
use App\Http\Resources\DataProfile as DataProfileResource;
use App\Location;
use Illuminate\Http\Request;

class DataProfileController extends Controller
{
    public function get(Request $request){
        return DataProfileResource::collection(DataProfile::all());
    }

    public function locations(Request $request){
        return response()->json(Location::all());
    }

    public function cities(Request $request){
        return response()->json(City::all());
    }

    public function countries(Request $request){
        return response()->json(Country::all());
    }
}
