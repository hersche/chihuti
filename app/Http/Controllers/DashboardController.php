<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\DataSet;
use App\DataSetRow;

class DashboardController extends Controller
{
    //
    public function index(Request $request){
        $dataSets = DataSet::all();
        $dataSheet = null;
        if(empty($request->input('data_set'))){
            $dataSheet = DataSet::first();
        } else {
            $dataSheet = DataSet::where('name', $request->input('data_set'))->first();
        }

        $targetDate = Carbon::now()->subWeek(1);
        if(!empty($request->input('targetDate'))){
            $targetDate = Carbon::parse($request->input('targetDate'));
        }
        $dataSheetRows = DataSetRow::where('data_set_id',$dataSheet->id)->whereDate('started_at',$targetDate->toDateString())->get();
        //$content = strip_tags($content,'<br><?php>');
        return view('welcome')->with(compact('dataSets', 'dataSheetRows','targetDate'));
    }
}
