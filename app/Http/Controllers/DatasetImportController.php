<?php

namespace App\Http\Controllers;

use App\DataSet;
use App\Logics\DatasetParser\Covid19CssegiSandDataParser;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DatasetImportController extends Controller
{
    //
    public function parse(Request $request){
        set_time_limit ( 360 );
        if(empty($request->input('data_set'))){
            return response()->json(DataSet::all());
        }
        $client = new Client();
        $dataSet = DataSet::where('name',$request->input('data_set'))->where('type', DataSet::DATA_SET_TYPE_WEB_CSV)->first();
        $response = $client->get($dataSet->source_url);
        $class = 'App\Logics\DatasetParser\\'.$dataSet->parser;
        $covid19Parser = new $class();
        $fileContent = $response->getBody();
        //$fileContent = Storage::disk('local')->get('covid19_real_data_sample.csv');
        $lines = preg_split('/[\n\r]/', $fileContent);
        $parsedLines = $covid19Parser->parseAndStoreResult($request->input('data_set'),$lines);
        return response()->json($parsedLines);
    }
}
