<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    function get(Request $request) {
        return UserResource::collection(User::all());
    }
}
