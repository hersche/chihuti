<?php

namespace App\Http\Resources;

use App\DataSetRow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class DataProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data = ['data_set_names' => [],'data_sets' => [], 'sensor_instances' => []];
        foreach($this->dataProfileDataSetConnections as $dataSetConnection){
            $dataSet = $dataSetConnection->dataSet;
            $dataSetNameSlug = $dataSet->name;
            $dataSetRows = null;
            if($request->has('at')){
                $dataSetRows = DataSetRow::join('data_sets','data_sets.id','=','data_set_rows.data_set_id')->whereDate('started_at', Carbon::parse($request->input('at'))->toDateString())->where('data_set_id',$dataSet->id)->orderBy('order')->get();
            } else {
                // Temporary change until more ui
                //$dataSetRows = $dataSet->dataSetRows;
                $dataSetRows = DataSetRow::join('data_sets','data_sets.id','=','data_set_rows.data_set_id')->where('data_set_id',$dataSet->id)->orderBy('order')->get();
            }
            $data['data_set_names'][$dataSet->id] = $dataSet->name;
            foreach($dataSetRows as $dataSetRow){
                $happend = '0000-00-00';
                if($dataSetRow->started_at) {
                    $happend = $dataSetRow->started_at->toDateString();
                }
                if(!isset($data['data_sets'][$happend])){
                    $data['data_sets'][$happend] = [];
                }

                $lh = $this->locationHash($dataSetRow);
                if(!isset($data['data_sets'][$happend][$lh])){
                    $data['data_sets'][$happend][$lh] = [
                        'location_id' => $dataSetRow->location_id,
                        'city_id' => $dataSetRow->city_id,
                        'country_id' => $dataSetRow->country_id,
                    ];
                    $data['data_sets'][$happend][$lh]['data'] = [];
                }
                $data['data_sets'][$happend][$lh]['data'][] = [
                    'name' => $dataSetNameSlug,
                    'value' => $dataSetRow->value,
                    'appendix' => $dataSet->appendix,
                    'prefix' => $dataSet->prefix,
                    'position' => $dataSet->position
                ];
            }
        }
        return [
          'id' => $this->id,
          'name' => $this->name,
          'data' => $data
        ];
        //return parent::toArray($request);
    }

    private function locationHash($dataSetRow){
        $locationId = '0';
        if(!empty($dataSetRow->location_id)){
            $locationId = strval($dataSetRow->location_id);
        }
        $cityId = '0';
        if(!empty($dataSetRow->city_id)){
            $cityId = strval($dataSetRow->city_id);
        }
        $countryId = '0';
        if(!empty($dataSetRow->country_id)){
            $countryId = strval($dataSetRow->country_id);
        }
        return $locationId.'-'.$cityId.'-'.$countryId;
    }
}
