<?php

namespace App\Http\Resources;

use App\DataSetRow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class Sensor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'sensors' => [

            ]
        ];
    }

    private function locationHash($dataSetRow){
        $locationId = '0';
        if(!empty($dataSetRow->location_id)){
            $locationId = strval($dataSetRow->location_id);
        }
        $cityId = '0';
        if(!empty($dataSetRow->city_id)){
            $cityId = strval($dataSetRow->city_id);
        }
        $countryId = '0';
        if(!empty($dataSetRow->country_id)){
            $countryId = strval($dataSetRow->country_id);
        }
        return $locationId.'-'.$cityId.'-'.$countryId;
    }
}
