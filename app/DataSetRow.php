<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSetRow extends Model
{
    protected $fillable = [
        'id', 'value', 'data_set_id','location_id','city_id','country_id', 'started_at', 'ended_at'
    ];
    protected $dates = [
        'started_at',
    ];
    public function dataSet(){
        return $this->belongsTo('App\DataSet','data_set_id', 'id');
    }

    public function country(){
        return $this->belongsTo('App\Country','country_id', 'id');
    }

    public function city(){
        return $this->belongsTo('App\City','city_id', 'id');
    }

    public function location(){
        return $this->belongsTo('App\Location','location_id', 'id');
    }
}
