<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'id', 'name', 'zip_code', 'city_id', 'country_id','long', 'lat'
    ];

    public function city(){
        return $this->belongsTo('App\City','city_id', 'id');
    }

    public function country(){
        return $this->belongsTo('App\Country','country_id', 'id');
    }
}
