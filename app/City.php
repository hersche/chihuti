<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'id', 'name','admin_name','long','lat', 'iso_2', 'iso_3', 'zip_code', 'country_id', 'province_id'
    ];

    public function province(){
        return $this->belongsTo('App\Provinc','province_id', 'id');
    }

    public function country(){
        return $this->belongsTo('App\Country','country_id', 'id');
    }
}
