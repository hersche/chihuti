<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $fillable = [
        'id', 'name', 'prefix', 'appendix', 'device_id', 'public', 'virtual','body_related'
    ];

}
