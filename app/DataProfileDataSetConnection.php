<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataProfileDataSetConnection extends Model
{
    protected $fillable = [
        'id', 'data_set_id', 'data_profile_id', 'cut_under_value', 'cut_above_value', 'start_at', 'ended_at'
    ];

    public function dataSet(){
        return $this->belongsTo('App\DataSet','data_set_id', 'id');
    }

    public function dataProfile(){
        return $this->belongsTo('App\DataProfile','data_profile_id', 'id');
    }
}
