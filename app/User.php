<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'bio', 'avatar', 'background', 'roles', 'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function level() {
        $roles = explode(',',$this->roles);
        $foundVal = 0;
        foreach ($roles as $role){
            $splitRole = explode(':',$role);
            if(empty($splitRole[1])){
                continue;
            }
            $level = intval($splitRole[1]);
            if($foundVal<$level){
                $foundVal = $level;
            }
        }
        return $foundVal;
    }

    public function avatar()
    {
        if (empty($this->avatar)) {
            return "img/404/avatar.png";
        }
        return $this->avatar;
    }

    public function background()
    {
        if (empty($this->background)) {
            return "img/404/background.png";
        }
        return $this->background;
    }

}
