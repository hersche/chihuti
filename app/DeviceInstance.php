<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceInstance extends Model
{
    protected $fillable = [
        'id', 'alias', 'description', 'device_id', 'public'
    ];

    public function device(){
        return $this->belongsTo('App\Device','device_id', 'id');
    }
}
