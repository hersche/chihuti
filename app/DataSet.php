<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSet extends Model
{
    const DATA_SET_TYPE_WEB_CSV = 'web_csv';
    const DATA_SET_TYPE_MANUAL = 'manual';

    protected $fillable = [
        'id', 'name', 'appendix', 'prefix', 'source_url', 'parser', 'order', 'type'
    ];

    public function dataSetRows()
    {
        return $this->hasMany('App\DataSetRow',"data_set_id","id");
    }

    public function dataProfile(){
        // TODO test!
        return $this->hasOneThrough('App\DataProfile', 'App\DataProfileDataSetConnection','data_set_id', 'id', 'id', 'data_profile_id');
    }
}
