<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorInstance extends Model
{
    protected $fillable = [
        'id', 'alias', 'public','user_id', 'sensor_id', 'user_id', 'device_instance_id'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id', 'id');
    }

    public function sensor(){
        return $this->belongsTo('App\Sensor','sensor_id', 'id');
    }

    public function values()
    {
        return $this->hasMany('App\SensorValue',"sensor_instance_id","id");
    }
}
