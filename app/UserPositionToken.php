<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPositionToken extends Model
{
    protected $fillable = [
        'id','api_token', 'user_id', 'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id', 'id');
    }

}
