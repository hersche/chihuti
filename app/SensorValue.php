<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorValue extends Model
{
    protected $fillable = [
        'id', 'value', 'sensor_instance_id', 'started_at', 'ended_at'
    ];

    protected $dates = [
        'started_at',
        'ended_at'
    ];

    public function sensorInstance(){
        return $this->belongsTo('App\SensorInstance','sensor_instance_id', 'id');
    }
}
