<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataProfileSensorInstanceConnection extends Model
{
    protected $fillable = [
        'id', 'sensor_instance_id', 'data_profile_id', 'cut_under_value', 'cut_above_value', 'start_at', 'ended_at'
    ];

    public function sensorInstance(){
        return $this->belongsTo('App\SensorInstance','sensor_instance_id', 'id');
    }

    public function dataProfile(){
        return $this->belongsTo('App\DataProfile','data_profile_id', 'id');
    }
}
