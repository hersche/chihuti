<?php

namespace App\Console\Commands;

use App\City;
use App\Country;
use App\DataSet;
use App\Logics\DatasetParser\Covid19SwissDataDemographicsParser;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class SwissDataCityCompleterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:swisscitycompleter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //

        $client = new Client();
        $response = $client->get('https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/template.csv');
        $fileContent = $response->getBody();
        $demoParser = new Covid19SwissDataDemographicsParser();
        //$fileContent = Storage::disk('local')->get('covid19_real_data_sample.csv');
        $lines = preg_split('/[\n\r]/', $fileContent);
        $titleString = array_shift($lines);
        $country = Country::firstOrCreate(['name' => 'Switzerland']);
        foreach ($lines as $line){
            $splittedLine = explode(',', $line);
            $city = City::where(function ($query) use($country,$splittedLine) {
                return $query->where('country_id', $country->id)->where('iso_2',$splittedLine[2]);
            })->orWhere(function ($query) use($country,$splittedLine) {
                return $query->where('country_id', $country->id)->where('name',$splittedLine[3]);
            })->orWhere(function ($query) use($country,$splittedLine) {
                return $query->where('country_id', $country->id)->where('admin_name',$splittedLine[3]);
            })->orWhere(function ($query) use($country,$splittedLine) {
                return $query->where('lat', $splittedLine[4])->where('long', $splittedLine[5]);
            })->first();

            if(empty($city)){
                echo "new created".$splittedLine[2];
                $city = City::create(['iso_2' => $splittedLine[2], 'name' => $splittedLine[3], 'admin_name' => $splittedLine[3],'lat' => $splittedLine[4],'long' => $splittedLine[5],'country_id' => $country->id]);
            }
            $city->admin_name = $splittedLine[3];
            $city->iso_2 = $splittedLine[2];
            $city->lat = $splittedLine[4];
            $city->long = $splittedLine[5];
            $city->save();
        }
    }
}
