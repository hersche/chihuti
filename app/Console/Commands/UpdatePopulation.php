<?php

namespace App\Console\Commands;

use App\DataSet;
use App\Logics\DatasetParser\WorldwidePopulationParser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\Cloner\Data;

class UpdatePopulation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:population {country?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("Receive datasets");
        $country = $this->argument('country');
        $dataSets = DataSet::where('type', DataSet::DATA_SET_TYPE_MANUAL)->get();
        foreach ($dataSets as $dataSet){
            $this->info("Start process on dataset ".$dataSet->name.$country);
            $rawContent = Storage::get($dataSet->source_url);
            $lines = preg_split("/(\r\n|\n|\r)/",$rawContent);
            $class = 'App\Logics\DatasetParser\\'.$dataSet->parser;
            $parser = new $class(compact('country'));
            $parser->parseAndStoreResult($dataSet->name, $lines);
            $this->info("Done with ".$dataSet->name);
        }

    }
}
