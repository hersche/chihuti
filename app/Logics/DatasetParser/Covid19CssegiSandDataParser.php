<?php

namespace App\Logics\DatasetParser;

use App\City;
use App\Country;
use App\DataSet;
use App\DataSetRow;
use App\Location;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Covid19CssegiSandDataParser implements ParserInterface {

    const CSV_DELIMITER = ',';

    public function checkSource(): bool
    {
        // TODO: Implement checkSource() method.
    }

    public function parse($lines): array
    {
        $titles = explode(self::CSV_DELIMITER, array_shift($lines));
        $parsedLines = [];
        $cachedCountries = [];
        $cachedLocations = [];
        foreach ($lines as $line){
            $line = str_replace(['\'', '"'], '', $line);
            $splittedLine = explode(self::CSV_DELIMITER, $line);
            if(empty($splittedLine[1]) || !is_numeric($splittedLine[2]) || !is_numeric($splittedLine[3])){
                continue;
            }
            $nrOfColumns = sizeof($titles);
            if(empty($cachedCountries[$splittedLine[1]])) {
                $cachedCountries[$splittedLine[1]] = Country::firstOrCreate(['name' => $splittedLine[1],'long' => $splittedLine[3],'lat' => $splittedLine[2]]);
            }
            $locationHash = $splittedLine[0].$splittedLine[3].$splittedLine[2].$cachedCountries[$splittedLine[1]]->id;
            if(empty($cachedLocations[$locationHash])) {
                $country = $cachedCountries[$splittedLine[1]];
                // $cachedLocations[$locationHash] = City::where('name',$splittedLine[0])->first();
                $cachedLocations[$locationHash] = City::where(function ($query) use($country,$splittedLine) {
                    return $query->where('country_id', $country->id)->where('name',$splittedLine[0]);
                })->orWhere(function ($query) use($country,$splittedLine) {
                    return $query->where('country_id', $country->id)->where('admin_name',$splittedLine[0]);
                })->orWhere(function ($query) use($country,$splittedLine) {
                    return $query->where('lat', $splittedLine[2])->where('long', $splittedLine[3]);
                })->first();
                if(empty($cachedLocations[$locationHash]) && !empty($splittedLine[0])){
                    $cachedLocations[$locationHash] = City::create(['name' => $splittedLine[0],'admin_name' => $splittedLine[0],'long' => $splittedLine[3],'lat' => $splittedLine[2], 'country_id' => $cachedCountries[$splittedLine[1]]->id]);
                }
            }
            $city_id = null;
            if(!empty($cachedLocations[$locationHash])){
                $city_id = $cachedLocations[$locationHash]->id;
            }
            for($column=4; $nrOfColumns>$column;$column++){
                $parsedLines[] = [
                    'city_id' => $city_id,
                    'country_id' => $cachedCountries[$splittedLine[1]]->id,
                    'lat' => $splittedLine[2],
                    'long' => $splittedLine[3],
                    'started_at' => $titles[$column],
                    'value' => $splittedLine[$column]
                ];
            }
        }
        return $parsedLines;
    }

    public function parseAndStoreResult($dataSetName, $lines): array
    {
        $dataRows = $this->parse($lines);
        $dataSet = DataSet::firstOrCreate(['name' => $dataSetName]);
        $dataSetRows = [];
        foreach ($dataRows as $dataRow){
            if(empty($dataRow['value'])){
                // skip 0's as we can say in case of corona, when no entry exist, we have to assume that(?)
                continue;
            }
           // var_dump(Carbon::createFromFormat('m/d/y', $dataRow['started_at'])); die();
            $dataSetRows[] = DataSetRow::firstOrCreate(['value' => $dataRow['value'], 'city_id' => $dataRow['city_id'], 'country_id' => $dataRow['country_id'], 'data_set_id' => $dataSet->id, 'started_at' => Carbon::createFromFormat('m/d/y', $dataRow['started_at'])->endOfDay()->toDateTimeString()]);
        }
        return $dataSetRows;
    }
}
