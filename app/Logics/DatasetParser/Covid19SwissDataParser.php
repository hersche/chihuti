<?php

namespace App\Logics\DatasetParser;

use App\City;
use App\Country;
use App\DataSet;
use App\DataSetRow;
use App\Location;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Covid19SwissDataParser implements ParserInterface {

    const CSV_DELIMITER = ',';

    public function checkSource(): bool
    {
        // TODO: Implement checkSource() method.
    }

    public function parse($lines): array
    {
        $titles = explode(self::CSV_DELIMITER, array_shift($lines));
        $parsedLines = [];
        $country = Country::where('name','Switzerland')->first();
        foreach ($lines as $key => $line){
            $line = str_replace(['\'', '"'], '', $line);
            $splittedLine = explode(self::CSV_DELIMITER, $line);
            if(empty($splittedLine)){
                continue;
            }
           /* if(empty($splittedLine[0]) || !is_numeric($splittedLine[2]) || !is_numeric($splittedLine[3])){
                continue;
            } */
            $started_at = $splittedLine[0];
            $nrOfColumns = sizeof($splittedLine);
            for($column=1; $nrOfColumns>$column;$column++){
                if(empty($titles[$column])){
                    continue;
                }
                $city = City::where('iso_2',$titles[$column])->where('country_id',$country->id)->first();
                if(empty($splittedLine[$column]) || empty($city)){
                    continue;
                }
                $parsedLines[] = [
                    'city_id' => $city->id,
                    'country_id' => $country->id,
                    'lat' => $city->lat,
                    'long' => $city->long,
                    'started_at' => $started_at,
                    'value' => $splittedLine[$column]
                ];
            }
            //die();
        }

        return $parsedLines;
    }

    public function parseAndStoreResult($dataSetName, $lines): array
    {
        $dataRows = $this->parse($lines);
        $dataSet = DataSet::firstOrCreate(['name' => $dataSetName]);
        $dataSetRows = [];
        foreach ($dataRows as $dataRow){
            if(empty($dataRow['value'])){
                // skip 0's as we can say in case of corona, when no entry exist, we have to assume that(?)
                continue;
            }
            // var_dump(Carbon::parse($dataRow['started_at'])->endOfDay()->toDateTimeString()); die();
            $dataSetRows[] = DataSetRow::firstOrCreate(['value' => $dataRow['value'], 'city_id' => $dataRow['city_id'], 'country_id' => $dataRow['country_id'], 'data_set_id' => $dataSet->id, 'started_at' => Carbon::parse($dataRow['started_at'])->endOfDay()->toDateTimeString()]);
        }
        return $dataSetRows;
    }
}
