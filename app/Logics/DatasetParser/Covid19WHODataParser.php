<?php

namespace App\Logics\DatasetParser;

use App\City;
use App\Country;
use App\DataSet;
use App\DataSetRow;
use App\Location;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Covid19WHODataParser implements ParserInterface {

    const CSV_DELIMITER = ',';
    const CSV_COUNTRY = 1;

    public function checkSource(): bool
    {
        // TODO: Implement checkSource() method.
    }

    public function parse($lines): array
    {
        $titles = explode(self::CSV_DELIMITER, array_shift($lines));
        $parsedLines = [];
        $cachedCountries = [];
        $cachedLocations = [];
        foreach ($lines as $line){
            $line = str_replace(['\'', '"'], '', $line);
            $splittedLine = explode(self::CSV_DELIMITER, $line);
            if(empty($splittedLine[1]) || !is_numeric($splittedLine[2]) || !is_numeric($splittedLine[3])){
                continue;
            }
            $nrOfColumns = sizeof($titles);
            if(empty($cachedCountries[$splittedLine[1]])) {
                $cachedCountries[$splittedLine[1]] = Country::firstOrCreate(['name' => $splittedLine[self::CSV_COUNTRY]]);
            }
            for($column=3; $nrOfColumns>$column;$column++){
                $parsedLines[] = [
                    'country_id' => $cachedCountries[$splittedLine[1]]->id,
                    'started_at' => $titles[$column],
                    'value' => $splittedLine[$column]
                ];
            }
        }
        return $parsedLines;
    }

    public function parseAndStoreResult($dataSetName, $lines): array
    {
        $dataRows = $this->parse($lines);
        $dataSet = DataSet::firstOrCreate(['name' => $dataSetName]);
        $dataSetRows = [];
        foreach ($dataRows as $dataRow){
            if(empty($dataRow['value'])){
                // skip 0's as we can say in case of corona, when no entry exist, we have to assume that(?)
                continue;
            }
           // var_dump(Carbon::createFromFormat('m/d/y', $dataRow['started_at'])); die();
            $dataSetRows[] = DataSetRow::firstOrCreate(['value' => $dataRow['value'], 'country_id' => $dataRow['country_id'], 'data_set_id' => $dataSet->id, 'started_at' => Carbon::createFromFormat('m/d/y', $dataRow['started_at'])->endOfDay()->toDateTimeString()]);
        }
        return $dataSetRows;
    }
}
