<?php

namespace App\Logics\DatasetParser;

use App\City;
use App\Country;
use App\DataSet;
use App\DataSetRow;
use App\Location;
use Carbon\Carbon;
use Illuminate\Support\Str;

class WorldwidePopulationParser implements ParserInterface {

    const CSV_DELIMITER = ',';
    const CITY_NAME = 0;
    const CITY_ASCII = 1;
    const CITY_LAT = 2;
    const CITY_LNG = 3;
    const CITY_COUNTRY = 4;
    const CITY_ISO_2 = 5;
    const CITY_ISO_3 = 6;
    const CITY_ADMIN_NAME = 7;
    const CITY_POPULATION = 9;

    private $country;
    public function __construct($options)
    {
        $this->country = $options['country'];
    }

    public function checkSource(): bool
    {
        // TODO: Implement checkSource() method.
    }

    public function parse($lines): array
    {
        $titles = explode(self::CSV_DELIMITER, array_shift($lines));
        $parsedLines = [];
        $cachedCountries = [];
        $cachedLocations = [];
        foreach ($lines as $line){
            $line = str_replace(['\'', '"'], '', $line);
            $splittedLine = explode(self::CSV_DELIMITER, $line);
            if(empty($splittedLine[1]) || !is_numeric($splittedLine[2]) || !is_numeric($splittedLine[3])){
                continue;
            }
            $nrOfColumns = sizeof($titles);
            if(empty($cachedCountries[$splittedLine[self::CITY_COUNTRY]])) {
                $cachedCountries[$splittedLine[self::CITY_COUNTRY]] = Country::firstOrCreate(['name' => $splittedLine[self::CITY_COUNTRY]]);
            }
            if(empty($cachedLocations[$splittedLine[self::CITY_NAME]])) {
                $cachedLocations[$splittedLine[self::CITY_NAME]] = City::firstOrCreate(['name' => $splittedLine[self::CITY_NAME],'admin_name' => $splittedLine[self::CITY_ADMIN_NAME],'long' => $splittedLine[self::CITY_LNG],'lat' => $splittedLine[self::CITY_LAT],'iso_2' => $splittedLine[self::CITY_ISO_2], 'iso_3' => $splittedLine[self::CITY_ISO_3], 'country_id' => $cachedCountries[$splittedLine[self::CITY_COUNTRY]]->id]);
            }
            if(empty($this->country) || Str::lower($this->country) == Str::lower($splittedLine[self::CITY_COUNTRY]) ) {
                $parsedLines[] = [
                    'city_id' => $cachedLocations[$splittedLine[self::CITY_NAME]]->id,
                    'country_id' => $cachedCountries[$splittedLine[self::CITY_COUNTRY]]->id,
                    'value' => $splittedLine[self::CITY_POPULATION]
                ];
            }
        }
        return $parsedLines;
    }

    public function parseAndStoreResult($dataSetName, $lines): array
    {
        $dataRows = $this->parse($lines);
        echo "lines parsed";
        $dataSet = DataSet::firstOrCreate(['name' => $dataSetName]);
        $dataSetRows = [];
        foreach ($dataRows as $dataRow){
            if(empty($dataRow['value'])){
                // skip 0's as we can say in case of corona, when no entry exist, we have to assume that(?)
                continue;
            }
           // var_dump(Carbon::createFromFormat('m/d/y', $dataRow['started_at'])); die();
            $dataSetRows[] = DataSetRow::firstOrCreate(['value' => $dataRow['value'], 'city_id' => $dataRow['city_id'], 'country_id' => $dataRow['country_id'], 'data_set_id' => $dataSet->id]);
        }
        return $dataSetRows;
    }
}
