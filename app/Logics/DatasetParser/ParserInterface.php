<?php

namespace App\Logics\DatasetParser;

interface ParserInterface {
    public function checkSource():bool;
    public function parse($lines):array;
    public function parseAndStoreResult($dataSetName, $lines):array;
}
