<?php

namespace App\Logics\DatasetParser;

use App\City;
use App\Country;
use App\DataSet;
use App\DataSetRow;
use App\Location;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Covid19CssegiSandDataUSParser implements ParserInterface {

    const CSV_DELIMITER = ',';
    const CSV_CITY = 6;
    const CSV_CONCRETE_CITY = 5;
    const CSV_LAT = 8;
    const CSV_LONG = 9;

    public function checkSource(): bool
    {
        // TODO: Implement checkSource() method.
    }

    public function parse($lines): array
    {
        $titles = explode(self::CSV_DELIMITER, array_shift($lines));
        $parsedLines = [];
        $cachedLocations = [];
        $country = Country::firstOrCreate(['name' => 'US']);
        foreach ($lines as $line){

            $line = str_replace(['\'', '"'], '', $line);
            $splittedLine = explode(self::CSV_DELIMITER, $line);
            if(empty($splittedLine[self::CSV_CITY]) || !is_numeric($splittedLine[self::CSV_LONG]) || !is_numeric($splittedLine[self::CSV_LAT])){
                //echo "fiiiii".$line."\r\n";
                //var_dump($splittedLine); echo "\r\n";
                continue;
            }
            $nrOfColumns = sizeof($titles);
            $locationHash = $splittedLine[self::CSV_CITY].$country->id;
            if(empty($cachedLocations[$locationHash])) {
                // $cachedLocations[$locationHash] = City::where('name',$splittedLine[0])->first();
                $cachedLocations[$locationHash] = City::where(function ($query) use($country,$splittedLine) {
                    return $query->where('country_id', $country->id)->where('name',$splittedLine[self::CSV_CITY]);
                })->orWhere(function ($query) use($country,$splittedLine) {
                    return $query->where('country_id', $country->id)->where('admin_name',$splittedLine[self::CSV_CITY]);
                })->orWhere(function ($query) use($country,$splittedLine) {
                    return $query->where('lat', $splittedLine[self::CSV_LAT])->where('long', $splittedLine[self::CSV_LONG]);
                })->first();
                if(empty($cachedLocations[$locationHash]) && !empty($splittedLine[0])){
                    $name = $splittedLine[self::CSV_CITY];
                    if(!empty($splittedLine[self::CSV_CONCRETE_CITY])){
                        $name .= ' ('.$splittedLine[self::CSV_CONCRETE_CITY].')';
                    }
                    $cachedLocations[$locationHash] = City::create(['name' => $name,'admin_name' => $name,'long' => $splittedLine[self::CSV_LONG],'lat' => $splittedLine[self::CSV_LAT], 'country_id' => $country->id]);
                }
            }
            $city_id = null;
            if(!empty($cachedLocations[$locationHash])){
                $city_id = $cachedLocations[$locationHash]->id;
            }
            //echo "foooooo";
            for($column=11; $nrOfColumns>$column;$column++){
                //echo $splittedLine[$column];
                $parsedLines[] = [
                    'city_id' => $city_id,
                    'country_id' => $country->id,
                    'lat' => $splittedLine[self::CSV_LAT],
                    'long' => $splittedLine[self::CSV_LONG],
                    'started_at' => $titles[$column],
                    'value' => $splittedLine[$column]
                ];
            }
        }
        return $parsedLines;
    }

    public function parseAndStoreResult($dataSetName, $lines): array
    {
        $dataRows = $this->parse($lines);
        $dataSet = DataSet::firstOrCreate(['name' => $dataSetName]);
        $dataSetRows = [];
        foreach ($dataRows as $dataRow){
            if(empty($dataRow['value'])){
                // skip 0's as we can say in case of corona, when no entry exist, we have to assume that(?)
                continue;
            }
           // var_dump(Carbon::createFromFormat('m/d/y', $dataRow['started_at'])); die();
            $dataSetRows[] = DataSetRow::firstOrCreate(['value' => $dataRow['value'], 'city_id' => $dataRow['city_id'], 'country_id' => $dataRow['country_id'], 'data_set_id' => $dataSet->id, 'started_at' => Carbon::createFromFormat('m/d/y', $dataRow['started_at'])->endOfDay()->toDateTimeString()]);
        }
        return $dataSetRows;
    }
}
