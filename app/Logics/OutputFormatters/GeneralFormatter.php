<?php


namespace App\Logics\OutputFormatters;


use App\DataSetRow;
use App\Http\Resources\Sensor;
use App\SensorValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GeneralFormatter
{

    const JSON_DATE_FORMAT = 'y/m/d';

    public function format($startDate, $endDate)
    {
        $finalData = [];
        if (empty($startDate)) {
            $startDate = Carbon::now()->subDay();
            $endDate = Carbon::now();
        }
        $sensorValues = SensorValue::where('started_at', '<', $startDate)->where('ended_at', '<', $endDate)->whereHas('sensor', function ($query) {
            $query->where('user_id',Auth::id())->orWhere('public', true);
        })->get();
        foreach ($sensorValues as $sensorValue) {
            $locationHash = $this->locationHash($sensorValue);
            $sensorName = $sensorValue->sensorInstance->sensor->name;
            if (!isset($finalData[$sensorName])) {
                $finalData['sensors'][$sensorName] = [
                    'virtual' => $sensorValue->sensorInstance->sensor->virtual,
                    'prefix' => $sensorValue->sensorInstance->sensor->prefix,
                    'appendix' => $sensorValue->sensorInstance->sensor->appendix,
                    'alias' => $sensorValue->sensorInstance->alias,
                    'name' => $sensorName
                ];
            }
            if (!isset($finalData[$locationHash])) {
                $finalData[$locationHash] = [
                    'location_id' => $sensorValue->sensorInstance->sensor->location_id,
                    'city_id' => $sensorValue->sensorInstance->sensor->city_id,
                    'country_id' => $sensorValue->sensorInstance->sensor->country_id,
                    'gps_position_id' => $sensorValue->sensorInstance->sensor->gps_position_id,
                    'data' => []
                ];
            }
            $startDate = $sensorValue->started_at->format(self::JSON_DATE_FORMAT);
            if (!isset($finalData[$locationHash]['data'][$startDate])) {
                $finalData[$locationHash]['data'][$startDate] = [];
                $finalData[$locationHash]['data'][$startDate]['sensors'] = [];
                $finalData[$locationHash]['data'][$startDate]['datasets'] = [];
            }
            $finalData[$locationHash]['data'][$startDate]['sensors'][$sensorName] = [
                'started_at' => $sensorValue->started_at,
                'ended_at' => $sensorValue->ended_at,
                'value' => $sensorValue->value,
                'name' => $sensorName
            ];
        }
        $dataSetValues = DataSetRow::where('started_at', '<', $startDate)->where('ended_at', '<', $endDate)->get();
        foreach ($dataSetValues as $dataSetValue) {
            $locationHash = $this->locationHash($dataSetValue);
            $sensorName = $dataSetValue->dataSet->name;
            if (!isset($finalData[$sensorName])) {
                $finalData['sensors'][$sensorName] = [
                    'prefix' => $dataSetValue->dataSet->prefix,
                    'appendix' => $dataSetValue->dataSet->appendix,
                    'name' => $sensorName
                ];
            }
            if (!isset($finalData[$locationHash])) {
                $finalData[$locationHash] = [
                    'location_id' => $dataSetValue->dataSet->location_id,
                    'city_id' => $dataSetValue->dataSet->city_id,
                    'country_id' => $dataSetValue->dataSet->country_id,
                    'gps_position_id' => $dataSetValue->dataSet->gps_position_id,
                    'data' => []
                ];
            }
            $startDate = $dataSetValue->started_at->format(self::JSON_DATE_FORMAT);
            if (!isset($finalData[$locationHash]['data'][$startDate])) {
                $finalData[$locationHash]['data'][$startDate] = [];
                $finalData[$locationHash]['data'][$startDate]['sensors'] = [];
                $finalData[$locationHash]['data'][$startDate]['datasets'] = [];
            }
            $finalData[$locationHash]['data'][$startDate]['datasets'][$sensorName] = [
                'started_at' => $dataSetValue->started_at,
                'ended_at' => $dataSetValue->ended_at,
                'value' => $dataSetValue->value,
                'name' => $sensorName
            ];
        }
        return response()->json($finalData);
    }

    private function locationHash($dataSetRow)
    {
        $locationId = '0';
        if (!empty($dataSetRow->location_id)) {
            $locationId = strval($dataSetRow->location_id);
        }
        $cityId = '0';
        if (!empty($dataSetRow->city_id)) {
            $cityId = strval($dataSetRow->city_id);
        }
        $countryId = '0';
        if (!empty($dataSetRow->country_id)) {
            $countryId = strval($dataSetRow->country_id);
        }
        $gpsPositionId = '0';
        if (!empty($dataSetRow->gps_position_id)) {
            $gpsPositionId = strval($dataSetRow->gps_position_id);
        }
        return $locationId . '-' . $cityId . '-' . $countryId . '-' . $gpsPositionId;
    }
}
