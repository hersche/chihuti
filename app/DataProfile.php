<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataProfile extends Model
{
    protected $fillable = [
        'id', 'name', 'description'
    ];

    public function dataProfileDataSetConnections()
    {
        return $this->hasMany('App\DataProfileDataSetConnection',"data_profile_id","id");
    }

    public function dataProfileSensorInstanceConnections()
    {
        return $this->hasMany('App\DataProfileSensorInstanceConnection',"sensor_instance_id","id");
    }
}
