<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPosition extends Model
{
    protected $fillable = [
        'id','user_id', 'long', 'lat', 'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id', 'id');
    }

}
