<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(config("app.auth")=="oauth"){
    Route::get('/login', 'Auth\OauthClientController@oauthLogin')->name("login");
}

Route::middleware('auth:web', 'throttle:60,1')->group(function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name("logout");
    Route::post('/logout', 'Auth\LoginController@logout')->name("logout");
});

Route::get('/api/auth/callback', 'Auth\OauthClientController@oauthCallback');


Route::get('/api/auth/getOauthUser', 'Auth\OauthClientController@oauthGetUser');

Route::get('/api/auth/refreshOauthUser', 'Auth\OauthClientController@oauthRefreshUser');

Route::get('/parse', 'DatasetImportController@parse');

Route::get('/userdata/googlefit', 'GoogleFit@get');

Route::get('/data/dataprofiles', 'DataProfileController@get');
Route::get('/data/locations', 'DataProfileController@locations');
Route::get('/data/countries', 'DataProfileController@countries');
Route::get('/data/cities', 'DataProfileController@cities');
Route::get('/system-data/users', 'UserController@get');
Route::get('/fit', 'GoogleFit@get');

Route::get('/', 'DashboardController@index');
