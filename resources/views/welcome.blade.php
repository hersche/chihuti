<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui, minimum-scale=1.0">
        <!-- CSRF Token -->
        <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">
        @guest
            <meta id="loggedUserId" content="0">
        @else
            <meta id="loggedUserId" content="{{ Auth::id() }}">
        @endguest
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="/js/app.js" defer></script>
        <link href="/css/app.css" rel="stylesheet">
        <!-- Styles -->
        <style>

            #app {
                width: 100%;
                height: 100%;
            }
        </style>

    </head>
    <body>
<div id="app">
    <thesidebar></thesidebar>
    <v-flex fluid class="mx-auto mt-5">
        <router-view v-bind:mixconfig="mixconfig" v-bind:canloadmore="canloadmore"></router-view>
    </v-flex>
</div>
    </body>
</html>
