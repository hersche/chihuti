import Vue from 'vue'
import Vuex from 'vuex'
import {locationStore} from "./geo";
Vue.use(Vuex)
const axios = require('axios');
import EventBus from '../eventbus';

export const dataProfileStore = new Vuex.Store({
    state: {
        dataProfiles:[],
        timeSumUpDataProfiles:[],
        dataSetNames:[],
        dataProfileNames:[],
        resolved: false,
        loaded: false
    },
    getters: {
        getDataProfileByName: (state) => (name) => {
            //return undefined
            var u;
            if(state.dataProfiles!=[]){
                u = state.dataProfiles.find(u => u.dataProfiles == name)
            }
            return u
        },
        getDataProfileById: (state) => (id) => {
            var u;
            if(state.dataProfiles!=[]){
                u = state.dataProfiles.find(u => u.id == id)

            }
            return u
        },
        getDataProfileByLocationId: (state) => (id) => {
            var u;
            if(state.dataProfiles!=[]){
                var data_set = state.dataProfiles[0].data.data_sets[0]
            }
            return u
        },
        refreshNameLookup: (state) => () => {
            var u;
            var newData = [];
            state.dataSetNames = new Object()
            state.dataProfileNames = []
            for(var el in state.dataProfiles){
                for(var e in el){
                    if(state.dataProfiles[el][e]==undefined){
                        continue
                    }
                    if(state.dataProfileNames.includes(state.dataProfiles[el][e].name) == false){
                        state.dataProfileNames.push(state.dataProfiles[el][e].name)
                    }
                    for(var n in state.dataProfiles[el][e].data.data_set_names){
                        var nn = state.dataProfiles[el][e].data.data_set_names[n]
                        if(state.dataSetNames[e] == undefined){
                            state.dataSetNames[e] = []
                        }
                        if(state.dataSetNames[e].includes(nn) == false) {
                            //console.log("sdhfli",nn)
                            state.dataSetNames[e].push(nn)
                        }
                    }
                    console.log('huhuhu', state.dataSetNames)
                    /*
                    for(var dateTime in state.dataProfiles[el][e].data.data_sets) {
                        for(var loc in state.dataProfiles[el][e].data.data_sets[dateTime]) {
                            for(var inData in state.dataProfiles[el][e].data.data_sets[dateTime][loc].data) {
                                if(state.dataSetNames[state.dataProfiles[el][e].name] == undefined){
                                    state.dataSetNames[state.dataProfiles[el][e].name] = []
                                }

                                if( state.dataSetNames[state.dataProfiles[el][e].name].includes(state.dataProfiles[el][e].data.data_sets[dateTime][loc].data[inData].name) == false){
                                    state.dataSetNames[state.dataProfiles[el][e].name].push(state.dataProfiles[el][e].data.data_sets[dateTime][loc].data[inData].name)
                                }
                            }
                        }
                    }

                     */
                    /*
                    for(var dateTime in state.dataProfiles[el][e].data.data_sets) {dataProfileName

                        for(var position in state.dataProfiles[el][e].data.data_sets[dateTime]) {
                            var tmpData
                            for(var setName in state.dataProfiles[el][e].data.data_sets[dateTime][position]) {
                                tmpData = state.dataProfiles[el][e].data.data_sets[dateTime][position][setName]
                            }
                            if (newData[position] == undefined) {
                                newData[position] = []
                                newData[position]['location_id'] = state.dataProfiles[el][e].data.data_sets[dateTime][position].location_id
                                newData[position]['country_id'] = state.dataProfiles[el][e].data.data_sets[dateTime][position].country_id
                                newData[position]['city_id'] = state.dataProfiles[el][e].data.data_sets[dateTime][position].city_id
                                newData[position]['data'] = []
                            }
                            newData[position]['data'][dateTime] = tmpData;
                        }
                    }*/
                }
            }
            return state.dataSetNames
        },
        receiveDataProfiles: (state) => () => {
            state.loaded = false
            axios.get("/data/dataprofiles",{})
                .then(function (response) {
                    dataProfileStore.commit("setDataProfiles",response.data)
                    dataProfileStore.getters.refreshNameLookup()
                    state.loaded = true
                    EventBus.$emit('DATA_PROFILES_RECEIVED', response.data);

                    return response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
    },
    mutations: {
        setDataProfiles(state,dataProfiles){
            state.dataProfiles = dataProfiles
        },
        addDataProfile(state,dataProfile){
            state.dataProfiles.push(dataProfile)
        },
    }
})
