import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const axios = require('axios');
const EMPTY_USER = {id:0,name:"None",admin:false,avatar:'/img/404/avatar.png',background:'/img/404/background.png',roles:[]}
export const userStore = new Vuex.Store({
    state: {
        users:[],
        loginId:0
    },
    getters: {
        getUserByName: (state) => (name) => {
            //return undefined
            var u;
            if(state.users!=[]){
                u = state.users.find(u => u.users == name)
            }
            if(u == undefined){
                return EMPTY_USER
            }
            return u
        },
        getUserById: (state) => (id) => {
            var u;
            if(state.users!=[]){
                u = state.users.find(u => u.id == id)
            }
            if(u == undefined){
                return EMPTY_USER
            }
            return u
        },
        receiveUsers: (state) => () => {
            axios.get("/system-data/users",{})
                .then(function (response) {
                    userStore.commit("setUsers",response.data.data)
                    return response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
    },
    mutations: {
        setUsers(state,users){
                state.users = users
                state.users.forEach(function (el, i) {
                    if (el.you) {
                        state.loginId = el.id;
                    }
                })

        },
        addUser(state,location){
            state.users.push(location)
        },
    }
})
