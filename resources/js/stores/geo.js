import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const axios = require('axios');

export const locationStore = new Vuex.Store({
    state: {
        dataLocations:[]
    },
    getters: {
        getLocationByName: (state) => (name) => {
            //return undefined
            var u;
            if(state.dataLocations!=[]){
                u = state.dataLocations.find(u => u.dataLocations == name)
            }
            return u
        },
        getLocationById: (state) => (id) => {
            var u;
            if(state.dataLocations!=[]){
                u = state.dataLocations.find(u => u.id == id)
            }
            return u
        },
        receiveLocations: (state) => () => {
            axios.get("/data/locations",{})
                .then(function (response) {
                    locationStore.commit("setLocations",response.data)
                    return response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
    },
    mutations: {
        setLocations(state,locations){
            state.dataLocations = locations
        },
        addUser(state,location){
            state.dataLocations.push(location)
        },
    }
})

export const countryStore = new Vuex.Store({
    state: {
        countries:[]
    },
    getters: {
        getCountryByName: (state) => (name) => {
            //return undefined
            var u;
            if(state.countries!=[]){
                u = state.countries.find(u => u.countries == name)
            }
            return u
        },
        getCountryById: (state) => (id) => {
            var u;
            if(state.countries!=[]){
                u = state.countries.find(u => u.id == id)
            }
            if(u==undefined){
                u={id:0,name:"None",admin:false,avatar:'/img/404/avatar.png',background:'/img/404/background.png'}
                //u=new User(0,"None","/img/404/avatar.png","/img/404/background.png","None-profile",{},"",false)
            }
            return u
        },
        receiveCountries: (state) => () => {
            axios.get("/data/countries",{})
                .then(function (response) {
                    countryStore.commit("setCountries",response.data)
                    return response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
    },
    mutations: {
        setCountries(state,countries){
            state.countries = countries
        },
        addCountry(state,country){
            state.countries.push(country)
        },
    }
})

export const cityStore = new Vuex.Store({
    state: {
        cities:[]
    },
    getters: {
        getCityByName: (state) => (name) => {
            //return undefined
            var u;
            if(state.cities!=[]){
                u = state.cities.find(u => u.cities == name)
            }
            return u
        },
        getCityById: (state) => (id) => {
            var u;
            if(state.cities!=[]){
                u = state.cities.find(u => u.id == id)
            }
            return u
        },
        receiveCities: (state) => () => {
            axios.get("/data/cities",{})
                .then(function (response) {
                    cityStore.commit("setCities",response.data)
                    return response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
    },
    mutations: {
        setCities(state,cities){
            state.cities = cities
        },
        addCity(state,city){
            state.cities.push(city)
        },
    }
})
