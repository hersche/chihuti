const axios = require('axios');
var loadedLangs = ['en'];
import {dataProfileStore} from './stores/stores.js';
import {locationStore,countryStore,cityStore} from './stores/geo.js';
import {userStore} from './stores/system'
import Vue from 'vue'
import Router from 'vue-router'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import 'material-icons/iconfont/material-icons.css';
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import 'leaflet/dist/leaflet.css';

Vue.use(Vuetify)

Vue.use(Vuex)
Vue.use(Router)
Vue.use(VueI18n)

var homeComponent = Vue.component('overview', require("./components/Home.vue").default);
var googleFitComponent = Vue.component('googlefit', require("./components/GoogleFit.vue").default);
var sidebarComp = Vue.component('thesidebar', require("./components/Navigation.vue").default);

const routes = [
    {path: '/', component: homeComponent},
    {path: '/fit', component: googleFitComponent},
]

document.addEventListener("DOMContentLoaded", function(){


    var app = new Vue({
        vuetify: new Vuetify(),
        data: {
            appname: process.env.MIX_APP_NAME,
            mixconfig: process.env,
            search: undefined,
            treecatptions: undefined,
            canloadmore: true,
            baseUrl: '',
            alertshown: false,
            alerttext: '',
            alertcolor: 'success'
        },
        router: new Router({
            routes,
            scrollBehavior(to, from, savedPosition) {
                return {x: 0, y: 0}
            }
        }),
        watch: {
            $route(to, from) {
            }
        },
        methods: {
            alert(msg, type = "green", icon = '') {
                console.log("alert-method")
                this.alertshown = true
                this.alerttext = msg
                this.alertcolor = type
                // this.$vs.notify({title:msg,text:'',icon:icon,color:type,position:'bottom-center'})
            }
        },
        components: {
            'thesidebar': sidebarComp,
        },
    }).$mount('#app')

    locationStore.getters.receiveLocations()
    countryStore.getters.receiveCountries()
    cityStore.getters.receiveCities()
    dataProfileStore.getters.receiveDataProfiles()
    userStore.getters.receiveUsers()
})
