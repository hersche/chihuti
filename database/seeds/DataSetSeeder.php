<?php

use Illuminate\Database\Seeder;
use App\DataSet;
use App\DataProfile;
use App\DataProfileDataSetConnection;

class DataSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $d1 = DataSet::create(['name' => 'COVID19-deaths', 'appendix' => 'death people', 'source_url' => 'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv', 'parser' => 'Covid19CssegiSandDataParser', 'order' => 6, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d2 = DataSet::create(['name' => 'COVID19-recovered', 'appendix' => 'recovered people', 'source_url' => 'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv', 'parser' => 'Covid19CssegiSandDataParser', 'order' => 3, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d3 = DataSet::create(['name' => 'COVID19-confirmed', 'appendix' => 'confirmed people', 'source_url' => 'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv', 'parser' => 'Covid19CssegiSandDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d4 = DataSet::create(['name' => 'COVID19-US-confirmed', 'appendix' => 'confirmed people (US)', 'source_url' => 'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv', 'parser' => 'Covid19CssegiSandDataUSParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d5 = DataSet::create(['name' => 'COVID19-WHO-confirmed', 'appendix' => 'confirmed people (WHO)', 'source_url' => 'https://github.com/CSSEGISandData/COVID-19/raw/master/who_covid_19_situation_reports/who_covid_19_sit_rep_time_series/who_covid_19_sit_rep_time_series.csv', 'parser' => 'Covid19WHODataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);

        $dataProfile = DataProfile::create(['name' => 'COVID19 by John Hopkins']);
        DataProfileDataSetConnection::create(['data_set_id' => $d1->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d2->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d3->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d4->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d5->id, 'data_profile_id' => $dataProfile->id]);

        $d1 = DataSet::create(['name' => 'confirmed', 'appendix' => 'confirmed people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/covid19_cases_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d2 = DataSet::create(['name' => 'released', 'appendix' => 'released people', 'source_url' => 'https://raw.githubusercontent.com/daenuprobst/covid19-cases-switzerland/master/covid19_released_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d3 = DataSet::create(['name' => 'hospitalised', 'appendix' => 'hospitalised people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/covid19_hospitalized_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d4 = DataSet::create(['name' => 'fatalities', 'appendix' => 'fatalised people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/covid19_fatalities_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d5 = DataSet::create(['name' => 'vent', 'appendix' => 'vent people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/covid19_vent_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d6 = DataSet::create(['name' => 'cases', 'appendix' => 'cases people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/raw/master/covid19_cases_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d7 = DataSet::create(['name' => 'tested', 'appendix' => 'tested people', 'source_url' => 'https://github.com/daenuprobst/covid19-cases-switzerland/blob/master/covid19_tested_switzerland_openzh.csv', 'parser' => 'Covid19SwissDataParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_WEB_CSV]);
        $d8 = DataSet::create(['name' => 'Population', 'appendix' => 'people', 'source_url' => 'data/simplemaps_worldcities_basicv1.6/worldcities.csv', 'parser' => 'WorldwidePopulationParser', 'order' => 9, 'type' => DataSet::DATA_SET_TYPE_MANUAL]);
        $dataProfile = DataProfile::create(['name' => 'Swiss COVID19']);
        DataProfileDataSetConnection::create(['data_set_id' => $d1->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d2->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d3->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d4->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d5->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d6->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d7->id, 'data_profile_id' => $dataProfile->id]);
        DataProfileDataSetConnection::create(['data_set_id' => $d8->id, 'data_profile_id' => $dataProfile->id]);
    }
}
