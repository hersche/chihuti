<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceAndSensorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->double('long')->nullable(); // to remove
            $table->double('lat')->nullable(); // to remove
            $table->bigInteger('gps_position_id')->nullable();
            $table->timestamps();
        });

        Schema::create('provinces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->double('long')->nullable(); // to remove
            $table->double('lat')->nullable(); // to remove
            $table->bigInteger('country_id');
            $table->bigInteger('gps_position_id')->nullable();
            $table->timestamps();
        });

        // For future, TODO a rewrite for it
        Schema::create('gps_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('long')->nullable();
            $table->double('lat')->nullable();
            $table->timestamps();
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('admin_name');
            $table->double('long'); // to remove
            $table->double('lat'); // to remove
            $table->text('zip_code')->nullable();
            $table->string('iso_2')->nullable();
            $table->string('iso_3')->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('province_id')->nullable();
            $table->bigInteger('gps_position_id')->nullable();
            $table->timestamps();
        });

        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->double('long'); // to remove
            $table->double('lat'); // to remove
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('gps_position_id')->nullable();
            $table->timestamps();
        });

        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('model')->nullable();
            $table->text('description')->nullable();
            $table->boolean('wearable')->default(false);
            $table->timestamps();
        });

        Schema::create('device_instances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alias')->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('device_id');
            $table->boolean('public')->default(false);
            $table->timestamps();
        });

        Schema::create('device_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('device_instance_id');
            $table->bigInteger('user_id');
            $table->boolean('admin')->default(false);
            $table->timestamps();
        });

        Schema::create('sensor_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value');
            $table->bigInteger('sensor_instance_id');
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('location_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('gps_position_id')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();
        });

        /*
         * Concrete sensor for a user
         */
        Schema::create('sensor_instances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alias')->nullable();
            $table->bigInteger('user_id');
            $table->bigInteger('sensor_id');
            $table->bigInteger('device_instance_id');
            $table->boolean('public')->default(false);
            $table->bigInteger('color_id')->nullable();
            $table->timestamps();
        });

        Schema::create('sensor_instance_affect_data_set', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sensor_instance_id');
            $table->bigInteger('data_set_id');
            $table->string('operator');
            $table->boolean('reverse_calculation_values')->default(false);
            $table->timestamps();
        });

        /*
         * In a public list to choose. You will be able to fill them out with simple form, to have them in list
         */
        Schema::create('sensors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('prefix')->nullable();
            $table->string('appendix')->nullable();
            $table->bigInteger('device_id');
            $table->boolean('virtual')->default(false);
            $table->boolean('body_related')->default(false);
            $table->timestamps();
        });

        /**
         * Save a history of moving, also to be able to reconstruct older data
         */
        Schema::create('user_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('long'); // to delete
            $table->string('lat'); // to delete
            $table->bigInteger('gps_position_id')->nullable();
            $table->timestamps();
        });

        /*
         * In imagination, this will be used with existing gps-tracking-apps, therefore we make individual, simply random tokens
         */
        Schema::create('user_position_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('api_token');
            $table->timestamps();
        });

        Schema::create('data_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('data_profile_data_set_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('data_set_id');
            $table->bigInteger('data_profile_id');
            $table->double('cut_under_value')->nullable();
            $table->double('cut_above_value')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();
        });

        Schema::create('data_profile_sensor_instance_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sensor_instance_id');
            $table->bigInteger('data_profile_id');
            $table->double('cut_under_value')->nullable();
            $table->double('cut_above_value')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();
        });

        Schema::create('data_set_calculations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prefix')->nullable();
            $table->bigInteger('data_set_id_1')->nullable();
            $table->string('operator')->nullable();
            $table->bigInteger('data_set_id_2')->nullable();
            $table->string('appendix')->nullable();
            $table->timestamps();
        });

        Schema::create('colors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value')->unique();
            $table->timestamps();
        });

        Schema::create('data_sets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('prefix')->nullable();
            $table->string('appendix')->nullable();
            $table->string('source_url')->nullable();
            $table->bigInteger('order')->default(0);
            $table->string('parser')->nullable();
            $table->string('type')->default('');
            $table->bigInteger('color_id')->nullable();
            $table->timestamps();
        });

        Schema::create('data_set_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value');
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('location_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('data_set_id');
            $table->bigInteger('gps_position_id')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gps_positions');
        Schema::dropIfExists('data_set_calculations');
        Schema::dropIfExists('data_profile_sensor_instance_connections');
        Schema::dropIfExists('data_profile_data_set_connections');
        Schema::dropIfExists('user_positions');
        Schema::dropIfExists('user_position_tokens');
        Schema::dropIfExists('sensor_instance_affect_data_set');
        Schema::dropIfExists('data_profiles');
        Schema::dropIfExists('data_sets');
        Schema::dropIfExists('data_set_rows');
        Schema::dropIfExists('sensor_instances');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('device_users');
        Schema::dropIfExists('devices');
        Schema::dropIfExists('device_instances');
        Schema::dropIfExists('sensor_values');
        Schema::dropIfExists('sensors');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('cities');
    }
}
