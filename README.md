## Chihuti

Chichuti combines some targets:
- Being able to track personal body-data and sensor-data in general
- Import datasets (Covid-cases atm, but allow same time weather-data for example)
- Present the data through a profile-layer to present charts, maps etc

For the currently-running #codevscovid19, the idea is mostly concentrated on Covid19.

Therefore, the concentrations are on:
- Body-temperature-sensors and similiar (heartrate is may interessting)
- Location-support, flags for body_related and wearable
- Parser for Covid-related-sources (WHO, CDC, ECDC,...)

The origin idea came from measure the outside, our enviroment, to may be able seeing more correlations between things (with gas-sensors for example).
However, i would like to create the idea that flexible, that it can cover multiple scenarios.

For a later use, extending it to:
- ActivityPub: hold data up-to-data between instances
- Diffrent API-integrations, for fetching data from Mi Band or Fitbit or or or
- ...

## If you want to develop, here's a very-rough and fast-written howto-setup

You need to setup auther too: https://gitlab.com/hersche/auther

##### Build both enviroments

In root-folder

`docker-compose up`

##### Access enviroments

File-changes can be easily done on host, commands should be executed in the enviroments

`docker exec --user 0 -it chihuti-app /bin/bash`

`docker exec --user 0 -it auther-app /bin/bash`

In browser, after running the rest of the setup:

http://chihuti:8092

http://auther:8090

(localhost works too in theory, but it's about oauth2)

##### DB's-enviroments
`docker exec --user 0 -it chihuti-db /bin/bash` # Table: chihuti

`docker exec --user 0 -it auther-db /bin/bash` # Table: auther

user / password: root / defaultPassword

##### host:

`echo "127.0.0.1 chihuti" >> /etc/hosts`

`echo "127.0.0.1 auther" >> /etc/hosts`

##### in both projects (accessed enviroments):
copy configs by: `cp .env.example .env`

`php artisian migrate`

`php artisian db:seed`

`composer install`

`yarn install`

`yarn run dev`

if you change migrations: `php artisian migrate:reset` (and then migrate again)

##### auther (accessed enviroment):

`php artisian passport:install`

then install client:

`php artisian passport:client`
- userid: 1
- name: blaa
- callback: http://chihuti:8092/api/auth/callback

The command returns secret and id, put it to **OAUTH_CLIENT_ID** and **OAUTH_CLIENT_SECRET** in **Chihuti-project-.env-file**

default-users / passwords: admin / admin, admin1 / admin1, admin2 / admin2

### Docker-compose (redundant old info)

After a `docker-compose up --build` the following ip's are available:
- `172.43.0.2` for php (unimportant by ip), docker-name: chihuti-app (composer, npm, etc)
- `172.43.0.3` for nginx (docker-name: chihuti-nginx)
- `172.43.0.4` for mysql (docker-name: chihuti-db, enter in your db-manager, db: horain, user: root, password: defaultPassword)

To finish the installation after clone first time, enter the php's terminal by `docker-compose up --build` 
and `docker exec --user 0 -it chihuti-app /bin/bash`, both in the project's root-folder. After entering, type
- `composer install`
- `yarn install`

After change settings in `.env`-file starting with `MIX_`, do a `npm run dev` or `npm run prod`. Also for fresh installations and similar this
can make sense.
